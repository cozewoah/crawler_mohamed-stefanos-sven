import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Link {

    private URL initialLink;
    private ArrayList<URL> targetDomainLinks = new ArrayList<>();
    private ArrayList<URL> externalDomainLinks = new ArrayList<>();

    Link(String startingURLString) {
        try {
            this.initialLink = new URL(startingURLString);
            this.targetDomainLinks.add(this.initialLink);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ArrayList<URL> getTargetDomainLinks() {
        return this.targetDomainLinks;
    }

    public void save(ArrayList<URL> extractedLinks) throws InterruptedException {

        for (URL extractedLink : extractedLinks
        ) {
            if (hasSameDomainOrigin(extractedLink, this.initialLink) && hasNotBeenSaved(extractedLink, targetDomainLinks)) {
                this.targetDomainLinks.add(extractedLink);
                System.out.println("Saved targetDomainLinks: " + extractedLink);
            } else if (hasNotBeenSaved(extractedLink, externalDomainLinks) && !hasSameDomainOrigin(extractedLink, this.initialLink)) {
                this.externalDomainLinks.add(extractedLink);
                System.out.println("Saved externalDomainLinks: " + extractedLink);

            }
        }


    }


    public void printTargetDomainLinks() {
        for (URL url : this.targetDomainLinks) {
            System.out.println(url);
        }
    }

    public void printExternalDomainLinks() {
        for (URL url : this.externalDomainLinks) {
            System.out.println(url);
        }
    }

    public boolean hasNotBeenSaved(URL link, ArrayList<URL> thisLinkList) {
        if (thisLinkList.contains(link) == false) {
            return true;
        }
        return false;
    }

    public boolean hasSameDomainOrigin(URL extractedLink, URL targetLink) {
        if (extractedLink.getAuthority().equals(targetLink.getAuthority())) {
            return true;
        }
        return false;
    }

}
