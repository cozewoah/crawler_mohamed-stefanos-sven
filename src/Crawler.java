import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class Crawler {


    public static void scan(Link link) {
        try {
            while (true) {

                ArrayList<URL> extractedLinks = new ArrayList<>();

                for (URL linkBeingScanned : link.getTargetDomainLinks()) {

                    BufferedReader in = new BufferedReader(new InputStreamReader(linkBeingScanned.openStream()));
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        if (hrefTagIn(inputLine)) {
                            URL extractedURL = extractURL(inputLine);
                            if (extractedURL != null) {
                                extractedLinks.add(extractedURL);
                            }
                        }
                        if (MailCollector.validateEmailPattern(inputLine, MailCollector.VALID_EMAIL_ADDRESS_REGEX)) {
                            String mailAddress = MailCollector.extractAddress(inputLine, MailCollector.VALID_EMAIL_ADDRESS_REGEX);
                            MailCollector.foundAddresses.add(mailAddress);
                        }
                    }
                    in.close();

                }
                link.save(extractedLinks);
                if (link.getTargetDomainLinks().size() > 300) {
                    break;
                }
                TimeUnit.SECONDS.sleep(2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean hrefTagIn(String inputLine) {
        if (inputLine.contains("href")) {
            return true;
        }
        return false;
    }

    private static URL extractURL(String inputLine) {
        int httpIndex = inputLine.indexOf("http");
        String urlString = null;
        char quoteType;

        if (httpIndex > -1) {
            quoteType = getQuoteType(inputLine);
            urlString = inputLine.substring(httpIndex, inputLine.indexOf(quoteType, httpIndex));
        }

        if (urlString != null) {
            try {
                return new URL(urlString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private static char getQuoteType(String inputLine) {
        int quoteIndex = inputLine.indexOf("http") - 1;
        if (inputLine.charAt(quoteIndex) == '"') {
            return '"';
        } else return '\'';
    }
}
