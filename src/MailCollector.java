import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MailCollector {
    public static ArrayList<String> foundAddresses = new ArrayList<String>();

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", Pattern.CASE_INSENSITIVE);

    public static boolean validateEmailPattern(String inputLine, Pattern pattern) {
        Matcher matcher = pattern.matcher(inputLine);
        return matcher.find();
    }

    public static String extractAddress(String inputLine, Pattern pattern) {
        Matcher matcher = pattern.matcher(inputLine);
        matcher.find();
        return matcher.group();
    }

    public static void printEmails(){
        for (String mail : foundAddresses
             ) {
            System.out.println(mail);
        }
    }


}
