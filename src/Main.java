public class Main {

    public static void main(String[] args) {

        Link link = new Link("http://www.csbme.de");
        Crawler.scan(link);

        System.out.println("====================TargetDomainLinks====================");
        link.printTargetDomainLinks();
        System.out.println("====================ExternalDomainLinks====================");
        link.printExternalDomainLinks();
        System.out.println("====================E-Mails====================");
        MailCollector.printEmails();

    }

}
